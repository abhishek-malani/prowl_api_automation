package constants;

public interface prowlAPIConstants {
	
	String ENVIRONMENT_URL_PATH="http://staging.treebohotels.com";
	String REGISTER_PATH="/prowl/users/v3/register";
	String AUTH_TOKEN = "/prowl/users/google/login";
	String OAUTH_PLAYGROUND = "https://developers.google.com/oauthplayground/";

}
