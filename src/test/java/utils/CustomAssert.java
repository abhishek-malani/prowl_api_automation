package utils;

import org.testng.asserts.Assertion;
import org.testng.asserts.SoftAssert;

public class CustomAssert {
	  private static Assertion hardAssert = new Assertion();
	  private static SoftAssert softAssert = new SoftAssert();
	  
	  public static void hardAssert(boolean result) {
		  hardAssert.assertTrue(result);
		}
	  
	  public static void softAssert(boolean result) {
		  softAssert.assertTrue(result);  
		}
	  
	  public static void assertAll(){
		  softAssert.assertAll();
	  }
}
