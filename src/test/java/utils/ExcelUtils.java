package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.*;


public class ExcelUtils {

	private static XSSFSheet ExcelWSheet;
    private static XSSFWorkbook ExcelWBook;
    private static XSSFCell Cell;
    private static XSSFRow Row;
    private static String Path;
	
	
	/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
	/********************		METHODS			*********************/
	/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

	
    //This method is to set the File path and to open the Excel file, Pass Excel Path and Sheetname as Arguments to this method
    public static void setExcelFile(String Path,String SheetName) throws Exception {
    	ExcelUtils.Path=Path;
           try {
               // Open the Excel file
            FileInputStream ExcelFile = new FileInputStream(Path);
            
            // Access the required test data sheet
            ExcelWBook = new XSSFWorkbook(ExcelFile);
            ExcelWSheet = ExcelWBook.getSheet(SheetName);
            
            } catch (Exception e){
                throw (e);
            }
    }
    
    //Commit the changes after writing on a file
    public static void commitChanges() throws Exception {
    	FileOutputStream ExcelFile=null;
           try {
               // Open the Excel file
            ExcelFile = new FileOutputStream(new File(ExcelUtils.Path));
            ExcelWBook.write(ExcelFile);
            } catch (Exception e){
                throw (e);
            } finally{
            	ExcelFile.close();
            }
    }
    
  //Getting Row Count of Excel
  	public static int getRowCount() {
  		return ExcelWSheet.getLastRowNum();
  	}
  	
    
    //This method is to read the String test data from the Excel cell, in this we are passing parameters as Row num and Col num
    public static String getStringCellData(int RowNum, int ColNum) throws Exception{
           try{
        	  Cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);
        	  Cell.setCellType(Cell.CELL_TYPE_STRING);
              String CellData = Cell.getStringCellValue();
              return CellData;
              }
           catch(NullPointerException np)
           		{
        	   return "";
           		}
           catch (Exception e){
        	   //Reporter.log("Error while fetching cell data");
                e.printStackTrace();
                return "";
              }
    }
   
    
  //This method is to write in the Excel cell, Row num and Col num are the parameters
	public static void setCellData(String Result,  int RowNum, int ColNum) throws Exception    {
           try{
              Row  = ExcelWSheet.getRow(RowNum);
            Cell = Row.getCell(ColNum, Row.RETURN_BLANK_AS_NULL);
            if (Cell == null) {
                Cell = Row.createCell(ColNum);
                Cell.setCellValue(Result);
                } else {
                    Cell.setCellValue(Result);
                }
       
              }catch(Exception e){
                    throw (e);
            }
        }
    
    /**
     * Read From File
     * 
     */
    public static String readFile(String fileName) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(fileName));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append("\n");
                line = br.readLine();
            }
            return sb.toString();
        } finally {
            br.close();
        }
    }
	
}